# llvm-py: Python Bindings for LLVM #
llvm-py provides Python bindings for LLVM.

## Home page ##
https://github.com/AndrewBC/llvm-py

## NOTICE ##
This repository of llvm-py is inactive and present for historical context.
The defacto mainline fork from this repo, forked by Continuum Analytics, is here: https://github.com/llvmpy/llvmpy

I think you should use that one instead of this one.

## Dependency Versions ##
This package has only been tested with LLVM 2.9, and Python 2.7, (not Python 3.x).

## Quickstart ##
1.  Get 2.9 version of LLVM, build it. Make sure '--enable-pic' is passed to LLVM's 'configure'.
2.  Get llvm-py and install it:

```
$ git clone git@github.com:AndrewBC/llvm-py.git
$ cd llvm-py
$ python setup.py install
```

3.  See documentation at 'www/web/index.html' and examples under 'test'.

## LICENSE ##
llvm-py is distributed under the new BSD license, which is similar to the LLVM license itself.
See the file called LICENSE for the full license text.
